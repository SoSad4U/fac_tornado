import tornado.ioloop
import tornado.web


def fac(n):
    if n == 0:
        return 1
    return fac(n-1) * n

class MainHandler(tornado.web.RedirectHandler):
    def get(self):
        n=int(self.get_argument('n'))
        print(n)
        self.write({'res': fac(n)})

app = tornado.web.Application([
    (r"/", MainHandler)
])
app.listen(8000)
tornado.ioloop.IOLoop.current().start()
