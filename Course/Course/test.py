import json
import requests


def get(num):
    return (json.loads(requests.get(f'http://127.0.0.1:8000/?n={num}').text)).get('res')


def test_1():
    assert get(1) == 1

def test_2():
    assert get(7) == 5040

def test_3():
    assert  get(11) == 39916800
